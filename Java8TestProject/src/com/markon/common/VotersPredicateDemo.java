package com.markon.common;

import java.util.Arrays;
import java.util.List;

public class VotersPredicateDemo {

	public static void main(String[] args) {
		Person john = new Person("john",51);
		Person jane = new Person("Jane",17);
		
		List<Person> list = Arrays.asList(john,jane);
		list.forEach(System.out::println);
		System.out.println("****Valid Voters below****");
		
		//Java 8 using streams
		list.stream()
			.filter(p->isValidVoter(p.age))
			.forEach(System.out::println);
		// Java 8 using for each
//		list.forEach(x->{
//			if(isValidVoter(x.age)){
//				System.out.println(x);}
//				});
		
// 	 	Java 7		
//		for(Person p : list)
//		{
//			if(isValidVoter(p.age))
//				System.out.println(p);
//		}
		
	}
	
	public static boolean isValidVoter(int x)
	{
		if(x>=21)
			return true;
		return false;
	}
	
	static class Person
	{
		Person(String name,int age)
		{
			this.name=name;
			this.age=age;
		}
		String name;
		int age;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		@Override
		public String toString() {
			return "Person [name=" + name + ", age=" + age +  "]";
		}
	}

}
