package com.markon.common;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class SupplierDemo {

	public static void main(String[] args) {
		
		Supplier<String> sup = ()->"Java8sample";
		System.out.println(sup.get().length());
		
		Consumer<String> con = x->System.out.println(x.length());
		con.accept("Java8sample");
		
		System.out.println(maker(Employee::new));
	}

	private static Employee maker(Supplier<Employee> sup)
	{
		return sup.get();
	}

}

class Employee {
	  @Override
	  public String toString() {
	    return "A EMPLOYEE";
	  }
	}
