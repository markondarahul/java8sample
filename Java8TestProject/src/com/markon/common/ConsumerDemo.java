package com.markon.common;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class ConsumerDemo {

	public static void main(String[] args) {
			
		Consumer<String> c = x -> System.out.println(x.toLowerCase());;
		c.accept("Hello World");
		List<Person> list = Arrays.asList(new Person("John",38),new Person("Jane",30));
		printMe(list,p->System.out.println(p.age));
		Map<String,Integer> map = new HashMap<>();
		map.put("a",10);
		map.put("b",20);
		map.put("c",30);
		
		//Using forEach
		 list.forEach(x->System.out.println(x.name));
		 map.forEach((k,v)->{
			System.out.println(k+" is  "+v); 
			if("b".equals(k))
			{
				System.out.println("**B is here**");
			}
		 });
			// 	Using streams		
		//list.stream()
		//	.forEach(x->System.out.println(x.name+" "+x.age));
		 
		 
		
	}
	


	public static void printMe(List<Person> list,Consumer<Person> consumer)
	{
		for(Person p: list)
			consumer.accept(p);
	}
	
	


}
