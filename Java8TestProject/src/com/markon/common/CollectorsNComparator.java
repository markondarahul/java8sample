package com.markon.common;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CollectorsNComparator {

	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(1,2,3,4,56,7,8,91,76,12,24,26,32,35,68,69);
		System.out.println(list);
		//list.forEach(x->System.out.print(x+","));
		List<Integer> evenList = list.stream()
									.filter(x->x%2==0)
									.sorted(Comparator.reverseOrder())
									.collect(Collectors.toList());
		
		System.out.println(evenList);
		

	}

}
