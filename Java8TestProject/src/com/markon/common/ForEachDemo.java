package com.markon.common;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;



public class ForEachDemo {

	public static void main(String[] args) {
		
		java.util.List<String> names = new LinkedList<>();
		names.add("first");
		names.add("second");
		names.add("third");
		
		for(String s : names){
			
		 if(s.equals("second"))
			System.out.println(s);
		}
		
		System.out.println("*****Java 8********");
		names.stream()
		 .filter(s->s.equals("second"))
		 .forEach(System.out::println);
		//names.forEach(System.out::println);
		
		
		

//		Map<String,Integer> map = new HashMap<>();
//		map.put("a", 10);
//		map.put("b", 20);
//		map.put("c", 30);
//		map.put("d", 40);
//		
////		for(Map.Entry<String,Integer> entry : map.entrySet())
////		{
////			System.out.println(entry.getKey() +" has "+entry.getValue());
////			if(entry.getKey().equalsIgnoreCase("b"))
////				System.out.println("hello B");
////		}
//		
//		map.forEach((k,v)->{
//			System.out.println(k +" has "+v);
//			if("b".equals(k))
//				System.out.println("hello b");
//			
//		});
//		
//		map.forEach((k,v)->System.out.println(k+" has "+v));
		
		
		
		
		
	}

}
