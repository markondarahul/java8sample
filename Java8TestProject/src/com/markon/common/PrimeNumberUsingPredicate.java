package com.markon.common;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

public class PrimeNumberUsingPredicate {
	public static void main(String args[])
	{
		List<Integer> allNumbers= Arrays.asList(1,3,4,6,7,8,11,12,15);
		//allNumbers.forEach(System.out::print);
		List<Integer> odd=Arrays.asList(1,3,5,7,9,11,13,15,17,19,21,23,25);
		System.out.println(allNumbers);
		
		System.out.println("\n********Prime****");
		List<Integer> primeNumbers = new LinkedList<>() ;
//		for(int a: allNumbers)
//		{
//		 if(isPrime(a))
//			 primeNumbers.add(a);
//		}
		
		primeNumbers = filter(allNumbers,(n)->isPrime((int) n));
		
		primeNumbers.forEach(System.out::println);
		
		primeNumbers = filter(odd,n->isPrime((int)n));
		System.out.println("****Odd Prime***");
		System.out.println(primeNumbers);
		
	}
	
	public  static boolean isPrime(int n)
	{
		if (n==1 || n==0)
			return false;
		for(int i=2;i<=n/2;i++)
		{
			if(n%i==0)
				return false;
		}
		return true;
	}
	
	public static List<Integer> filter(List<Integer> list,Predicate p)
	{
		List<Integer> result = new LinkedList<>();
//		for(int n:list)
//		{
//			if(p.test(n))
//			{
//				result.add(n);
//			}
//		}
		list.stream()
			.filter(i->p.test(i))
			.forEach(i->result.add(i));
		return result;
	}
}
