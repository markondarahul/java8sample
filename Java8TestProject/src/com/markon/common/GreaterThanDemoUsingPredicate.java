package com.markon.common;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class GreaterThanDemoUsingPredicate {

	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(110,20,40,15,28,23,64,98,4,51,17);
		System.out.println(list);
		
//		list.stream()
//			.filter(x->x>50)
//			.forEach(x->System.out.println(x));
		Predicate<Integer> even = x->x%2==0;
		List<Integer> finalList =  list.stream()
				.filter(x->even.test(x))
				.collect(Collectors.toList());
System.out.println(finalList);
				//.ifPresent(System.out::println);
		
	}
	
	public static boolean isGreaterThan50(int x)
	{
		if (x>50)
			return true;
		return false;
	}
	
	public static boolean isEven(int x)
	{
		 if(x%2==0)
			 return true;
		return false;
	}

}
