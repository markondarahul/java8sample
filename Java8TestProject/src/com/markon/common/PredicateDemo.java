package com.markon.common;

import java.util.function.Predicate;

public class PredicateDemo {

	public static void main(String[] args) {
		
		Predicate<Integer> i = s->s<21;
		Predicate<Integer> j  = (s)-> s < 3;
		System.out.println(i.or(j).test(90));
		
		Patient joe = new Patient("Joe",50,Sex.MALE);
		Patient rachel = new Patient("Rachel",18,Sex.FEMALE);
		System.out.println(joe.name +" "+ joe.sex+" is "+joe.age);
		System.out.println(rachel.name +" is "+rachel.age);	
		if(i.test(joe.age))
			System.out.println("Joe is > 21");
		else {
			System.out.println("Joe is <");
		}
		if(i.test(rachel.age))
			System.out.println("Rachel is > 21");
		else
			System.out.println("Rachel is <");
		for(Sex s: Sex.values())
			System.out.println(s+" value is "+s.value);
		
		

	}
	
	static class Patient
	{
		Patient(String name,int age,Sex sex)
		{
			this.name=name;
			this.age = age;
			this.sex = sex;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
		
		public Sex getSex() {
			return sex;
		}
		public void setSex(Sex s) {
			this.sex = s;
		}


		private int age;
		private String name;
		private Sex sex;
	
		
	}
	public enum Sex
	{
		MALE(10),FEMALE(20);
		private int value;
		  Sex(int v)
		  {
			  this.value = v;
		  }
		
	}

}
